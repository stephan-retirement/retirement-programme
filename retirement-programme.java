package Stephan; // <- a really great package!

import java.util.Calendar;

import javax.swing.JOptionPane;

public class Retirement {

		public static final int RETIREMENT_AGE = 65;

		private static int currentAge; 
		
		private static int currentHour;
		
		public static void main(String [] args){

		        

		        
		        String currentAgeS = JOptionPane.showInputDialog(
					null, "Please insert your current age."
					);
		        
		        currentAge = Integer.parseInt( currentAgeS );
		        
		        System.out.println("Current Age = " + currentAge);
		        
				Calendar now = Calendar.getInstance();
		        currentHour = now.get(Calendar.HOUR_OF_DAY);
		        
		        if (currentAge <= RETIREMENT_AGE){
		            
		        	goToWork();
		        	
		        } else {
		            
		        	System.out.println("Hello Retirement");
		        	retireFromWork();
		            enjoyRetirement();
		            
		        }

		} // \ main

		public static void goToWork(){
		        
		        if (currentHour == 7) {
		            System.out.println("Alarm rings");
		            System.out.println("Wake up");
		            System.out.println("Have breakfast"); }
		        if (currentHour >= 8) {
		            System.out.println("Drive to work");}
		        if (currentHour >= 9) {
		            System.out.println("Say Hello to colleagues");
		            System.out.println("Start working");}
		        if (currentHour >= 12){
		            System.out.println("Have lunch");}
		        if (currentHour >= 13){
		            System.out.println("Back to work");}
		        if (currentHour >= 17){
		            System.out.println("Drive home");
		        }
		}
			
		public static void retireFromWork(){
		            System.out.println("Colleagues throw party");
		            System.out.println("Everyone eats cake");
		            System.out.println("You retire");
		        }

		public static void enjoyRetirement(){
		        if (currentHour == 7){
		            System.out.println("Alarm rings");
		            System.out.println("Switch off Alarm");
		            System.out.println("Go back to sleep");}
		        if (currentHour >= 9){
		            System.out.println("Wake up slowly");
		            System.out.println("Have a long breakfast");
	                    System.out.println("Help in housekeeping");
		            System.out.println("go for a walk/yoga");}
		        if (currentHour >= 12){
		            System.out.println("Have lunch");}
		        if (currentHour >= 13){
		            System.out.println("Take a Nap");}
		        if (currentHour >= 17){
		            System.out.println("Play with Grandkids");}
		        if (currentHour >= 19){
		            System.out.println("Have dinner");
		            System.out.println("Prepare for a tango evening");}
		        }
		}
